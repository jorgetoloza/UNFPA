import './../sass/styles.scss'; 

window.onbeforeunload = function(){ window.scrollTo(0,0); }

let lineScroll = document.querySelector('#intro #counter .line-scroll path');
let lineScrollLength = lineScroll.getTotalLength();
lineScroll.style['stroke-dasharray'] = lineScrollLength;
lineScroll.style['stroke-dashoffset'] = lineScrollLength;

let lastScroll = 0;

var introAnimation = new hoverEffect({
    parent: document.querySelector('#intro #sectionNext .bg'), 
    intensity: 0.1,
    angle: Math.PI,
    speedIn: 2.2,
    image1: 'assets/img/cover1_blur.png',
    image2: 'assets/img/cover2.png',
    displacementImage: 'assets/img/displacement.png',
    hover: false
}); 
document.getElementById('intro').addEventListener('scroll', (e) =>{
    let porcent = 1 - e.currentTarget.scrollTop/(e.currentTarget.scrollHeight-window.innerHeight);
    lineScroll.style['stroke-dashoffset'] = lineScrollLength*porcent;
    if(porcent == 0){
        document.querySelector('#intro #counter').classList.add('hide');
        introAnimation.next();
        
    }else{
        document.querySelector('#intro #counter').classList.remove('hide');  
        introAnimation.previous();
    }
         
});
window.onscroll = (e) =>{
    let scrollTop = document.querySelector('html').scrollTop;
    if(scrollTop == 0 && document.querySelector('body').classList.contains('full'))
        document.querySelector('body').classList.remove('full'); 

    lastScroll = scrollTop;  
};
document.getElementById('intro').addEventListener('mousemove', (e) =>{
    document.querySelector('#intro #counter .d3').style.transform = 'perspective(1000px) translate3d(4px, 5px, 0px) rotateX('+(e.pageX*0.01)+'deg) rotateY('+(e.pageY*0.02)+'deg) rotateZ(1deg)';
    document.querySelector('#intro #sectionNext .gradient').style.transform = 'perspective(1000px) scale(' + (1 + e.pageX / window.innerHeight) + ') translate3d(4px, 5px, 0px) rotateX('+(e.pageX*0.01)+'deg) rotateY('+(e.pageY*0.02)+'deg) rotateZ(1deg)';
    document.querySelector('#intro #sectionNext .gradient').style.opacity = e.pageY / window.innerHeight;
});
document.getElementById('gotoNext').addEventListener('click', (e) =>{
    document.querySelector('body').classList.add('full');
});
document.querySelector('#cover button').addEventListener('click', (e) =>{
    document.querySelector('html').scrollTop = window.innerHeight;
});
document.querySelector('#slider #nextSlide').addEventListener('click', (e) =>{
    let $slider = document.getElementById('slider');
    let $current = $slider.querySelector('.bg.active');
    $current.classList.remove('active');
    $current.nextElementSibling.classList.remove('next');
    if(!$current.nextElementSibling.classList.contains('bg')){
        let $first = $slider.querySelector('.bg:first-child');
        $first.classList.add('active');
        $first.nextElementSibling.classList.add('next');
    }else{
        $current.nextElementSibling.classList.add('active');
        $current.nextElementSibling.nextElementSibling.classList.add('next');
    }
});
var supportAnimation = new hoverEffect({
    parent: document.getElementById('support'), 
    intensity: 0.3,
    image1: 'assets/img/Feder_-43.png',
    image2: 'assets/img/UNFPA_Liberia_MIDWIVES_ElenaHeatherwick-11.png',
    displacementImage: 'assets/img/displacement.png',
    hover: false
});

document.querySelector('#support button:last-child').addEventListener('mouseenter', (e) =>{
    e.currentTarget.classList.add('active');
    e.currentTarget.previousElementSibling.classList.remove('active');    
    supportAnimation.next();
});
document.querySelector('#support button:last-child').addEventListener('mouseleave', (e) =>{
    e.currentTarget.classList.remove('active');
    e.currentTarget.previousElementSibling.classList.add('active');  
    supportAnimation.previous();
});

var controller = new ScrollMagic.Controller();
var tween = new TimelineMax ()
.add([
    TweenMax.to("#yamah flex .bg", 1, {transform: "translateY(-80%)", ease: Linear.easeNone}),
    TweenMax.to("#yamah .text", 1, {transform: "translateY(-600px)", ease: Linear.easeNone}),
    TweenMax.to("#yamah .line", 1, {transform: "translateY(300px)", ease: Linear.easeNone})
    
]);

var scene = new ScrollMagic.Scene({triggerElement: "#yamah", duration: 1000, offset: 150})
.setTween(tween)
.addTo(controller);
let $story = document.getElementById('story');
var sceneInfo = new ScrollMagic.Scene({
    triggerElement: $story,
    offset: -100
})
.setClassToggle($story, 'show')
.addTo(controller);